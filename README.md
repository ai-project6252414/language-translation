# Transformer Playground

Playground with exercises, libs and jupyter workbooks dedicated to the transformer architecture.

## Scripts

### Downloading S3 Data

copy_transformer_playground_data_from_s3.sh

Allows you to download the model and training data from the transformer_playground bucket from S3. 

Takes two parameters:

* S3Bucket: this is the relative path to the data or model you are trying to download. Path is hardcoded to be relative to transformer-playground S3 Bucket key.
* OutputDir: relative path to output dir. Path is hardcoded to be relative to /data folder in this project.

> `./scripts/copy_training_data_from_s3.sh data/coursera_attention_models_course/portugese_language_translation/ portugese_language_translation`