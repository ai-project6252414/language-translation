import tensorflow as tf


class CrossAttention(tf.keras.layers.Layer):
    def __init__(self, units: int):
        """_summary_

        Args:
            units (int): _description_
        """
        super().__init__()

        self._mha = (
            tf.keras.layers.MultiHeadAttention(
                key_dim=units,
                num_heads=1
            )
        )

        self._layernorm = tf.keras.layers.LayerNormalization()
        self._add = tf.keras.layers.Add()

    def call(self, context: tf.Tensor, target: tf.Tensor) -> tf.Tensor:
        """_summary_

        Args:
            context (tf.Tensor): Encoded sentence to translate
            target (tf.Tensor): The embedded shifted-to-the-right translation
        Returns:
            tf.Tensor: Cross attention between context and target
        """
        attn_output = self._mha(
            query=target,
            value=context
        )

        x = self._add([target, attn_output])
        x = self._layernorm(x)
        return x
