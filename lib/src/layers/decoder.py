import tensorflow as tf
from .cross_attention import CrossAttention


class Decoder(tf.keras.layers.Layer):
    def __init__(self, vocab_size: int, units: int):
        """_summary_

        Args:
            vocab_size (int): Size of the vocabulary
            units (int): Number of units in the LSTM Layer
        """
        super(Decoder, self).__init__()
        self._embedding = tf.keras.layers.Embedding(
            input_dim=vocab_size,
            output_dim=units,
            mask_zero=True
        )

        self._pre_attention_rnn = tf.keras.layers.LSTM(
            units=units,
            return_sequences=True,
            return_state=True
        )

        self._attention = CrossAttention(units)

        self._post_attention_rnn = tf.keras.layers.LSTM(
            units=units,
            return_sequences=True
        )

        self._output_layer = tf.keras.layers.Dense(
            units=vocab_size,
            activation=tf.nn.log_softmax
        )

    def call(self, context: tf.Tensor, target: tf.Tensor, state=None, return_state=False) -> tf.Tensor:
        """_summary_

        Args:
            context (tf.Tensor): Encoded sentence to translate
            target (tf.Tensor): The shifted-to-the-right translation
            state (_type_, optional): Hidden state of the pre-attention LSTM. Defaults to None.
            return_state (bool, optional): If set to true return the hidden states of the LSTM. Defaults to False.

        Returns:
            tf.Tensor: The log_softmax probabilityes of predicting a token.
        """
        x = self._embedding(target)
        x, hidden_state, cell_state = self._pre_attention_rnn(x, initial_state=state)
        x = self._attention(tf.cast(context, tf.float32), tf.cast(x, tf.float32))
        x = self._post_attention_rnn(x)
        logits = self._output_layer(x)

        if return_state:
            return logits, [hidden_state, cell_state]

        return logits
