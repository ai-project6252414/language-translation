import tensorflow as tf


class Encoder(tf.keras.layers.Layer):
    def __init__(self, vocab_size: int, units: int):
        """_summary_

        Args:
            vocab_size (int): _description_
            units (int): _description_
        """
        super(Encoder, self).__init__()
        self._embedding = tf.keras.layers.Embedding(
            input_dim=vocab_size,
            output_dim=units,
            mask_zero=True
        )

        self._rnn = tf.keras.layers.Bidirectional(
            merge_mode="sum",
            layer=tf.keras.layers.LSTM(
                units=units,
                return_sequences=True
            )
        )

    def call(self, context: tf.Tensor) -> tf.Tensor:
        """_summary_

        Args:
            context (tf.Tensor): The sentence to translate
        Returns:
            tf.Tensor: Encoded sentence to translate
        """
        x = self._embedding(context)
        x = self._rnn(x)
        return x
