import tensorflow as tf
from src.layers import Encoder, Decoder


class Translator(tf.keras.Model):
    def __init__(self, vocab_size, units):
        """_summary_

        Args:
            vocab_size (_type_): _description_
            units (_type_): _description_
        """
        super().__init__()
        self._encoder = Encoder(vocab_size, units)
        self._decoder = Decoder(vocab_size, units)

    def call(self, inputs: tuple[tf.Tensor, tf.Tensor]) -> tf.Tensor:
        """_summary_

        Args:
            inputs (tuple[tf.Tensor, tf.Tensor]): Tuple containing the context (sentence to translate) and the target (shifted-to-the-right translation)

        Returns:
            tf.Tensor: The log_softmax probabilities of predicting a particular token
        """
        context, target = inputs
        encoded_context = self._encoder(context)
        logits = self._decoder(encoded_context, target)
        return logits
