import tensorflow as tf


def test_cross_attention_layer(n_units, cross_attention_layer):
    assert isinstance(cross_attention_layer._mha, tf.keras.layers.MultiHeadAttention)
    assert cross_attention_layer._mha.key_dim == n_units
    assert cross_attention_layer._mha.num_heads == 1
    assert isinstance(cross_attention_layer._layernorm, tf.keras.layers.LayerNormalization)
    assert isinstance(cross_attention_layer._add, tf.keras.layers.Add)


def test_cross_attention_context_output(n_units,
                                        encoder_layer,
                                        cross_attention_layer,
                                        mock_sr_to_translate_tensor,
                                        mock_sr_translation_embed):
    encoder_output = encoder_layer(mock_sr_to_translate_tensor)
    cross_attention_result = cross_attention_layer(encoder_output, mock_sr_translation_embed)
    expected_shape = mock_sr_to_translate_tensor.shape + n_units
    assert cross_attention_result.shape == expected_shape
