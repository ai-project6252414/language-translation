import tensorflow as tf


def test_decoder_layer(vocab_size, n_units, decoder_layer):
    assert isinstance(decoder_layer._embedding, tf.keras.layers.Embedding)
    assert decoder_layer._embedding.input_dim == vocab_size
    assert decoder_layer._embedding.output_dim == n_units
    assert decoder_layer._embedding.mask_zero
    assert isinstance(decoder_layer._pre_attention_rnn, tf.keras.layers.LSTM)
    assert decoder_layer._pre_attention_rnn.units == n_units
    assert decoder_layer._pre_attention_rnn.return_sequences
    assert decoder_layer._pre_attention_rnn.return_state
    assert isinstance(decoder_layer._post_attention_rnn, tf.keras.layers.LSTM)
    assert decoder_layer._post_attention_rnn.units == n_units
    assert decoder_layer._post_attention_rnn.return_sequences
    assert isinstance(decoder_layer._output_layer, tf.keras.layers.Dense)
    assert decoder_layer._output_layer.units == vocab_size


def test_decoder_layer_output(vocab_size, encoder_layer,
                              mock_to_translate_tensor,
                              mock_sr_to_translate_tensor,
                              decoder_layer):
    encoder_output = encoder_layer(mock_to_translate_tensor)
    decoder_output = decoder_layer(encoder_output, mock_sr_to_translate_tensor)
    expected_shape = mock_sr_to_translate_tensor.shape + vocab_size
    assert decoder_output.shape == expected_shape
