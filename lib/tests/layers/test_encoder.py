import pytest
import tensorflow as tf
from src.layers import Encoder


@pytest.fixture
def encoder_params():
    return [
        (5, 32), (20, 64), (1000, 256), (15000, 512)
    ]


@pytest.fixture
def encoders_from_params(encoder_params):
    return [Encoder(vocab_size, n_units) for vocab_size, n_units in encoder_params]


def test_encoder_constructor(encoder_layer):
    assert (encoder_layer._embedding.mask_zero)
    assert isinstance(encoder_layer._embedding, tf.keras.layers.Embedding)
    assert isinstance(encoder_layer._rnn, tf.keras.layers.Bidirectional)
    assert isinstance(encoder_layer._rnn.layer, tf.keras.layers.LSTM)


def test_encoder_params(encoder_params, encoders_from_params):
    for i, param in enumerate(encoder_params):
        vocab_size, n_units = param
        cur_encoder = encoders_from_params[i]
        assert (cur_encoder._embedding.input_dim == vocab_size)
        assert (cur_encoder._embedding.output_dim == n_units)


def test_encoder_output(encoder_layer, mock_to_translate_tensor):
    encoder_output = encoder_layer(mock_to_translate_tensor)
    expected_shape = mock_to_translate_tensor.shape + encoder_layer._embedding.output_dim
    assert encoder_output.shape == expected_shape
