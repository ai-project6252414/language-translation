import pytest
from src.models.translator import Translator
from src.layers import Encoder, Decoder


@pytest.fixture
def translator(vocab_size, n_units) -> Translator:
    return Translator(vocab_size, n_units)


def test_translator(translator):
    assert isinstance(translator._encoder, Encoder)
    assert isinstance(translator._decoder, Decoder)


def test_translator_output(vocab_size,
                           mock_to_translate_tensor,
                           mock_sr_to_translate_tensor,
                           translator):
    logits = translator((mock_to_translate_tensor, mock_sr_to_translate_tensor))
    expected_shape = mock_sr_to_translate_tensor.shape + vocab_size
    assert logits.shape == expected_shape
