#! /usr/bin/bash

s3Bucket=$1
outputDir=$2

if [ -z $s3Bucket ]
then
    echo "S3 Bucket hasn't been specified"
    exit 1
fi

if [ -z $outputDir ]
then
    echo "Output Dir has not been specified"
    exit 1
fi

echo "S3 Bucket $s3Bucket"

aws s3 cp --recursive s3://transformer-playground/$s3Bucket ./data/$outputDir