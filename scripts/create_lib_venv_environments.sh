#! /usr/bin/bash

rm -rf lib/venv_language_translation
python3 -m venv venv_language_translation lib/venv_language_translation
source lib/venv_language_translation/bin/activate
pip3 install -r lib/requirements.txt