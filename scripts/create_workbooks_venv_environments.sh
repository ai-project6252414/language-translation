#! /usr/bin/bash

rm -rf workbooks/venv_workbooks
python3 -m venv venv_workbooks workbooks/venv_workbooks
source workbooks/venv_workbooks/bin/activate
pip3 install -r workbooks/requirements.txt