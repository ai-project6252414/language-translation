{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Basic Attention Lab"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "\n",
    "np.random.seed(42)\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Constants"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [],
   "source": [
    "HIDDEN_SIZE = 10\n",
    "ATTENTION_SIZE = 10\n",
    "INPUT_LENGTH = 5\n",
    "ENCODER_STATES = np.random.randn(INPUT_LENGTH, HIDDEN_SIZE)\n",
    "DECODER_STATES = np.random.randn(1, HIDDEN_SIZE)\n",
    "LAYER_1 = np.random.randn(2 * HIDDEN_SIZE, ATTENTION_SIZE)\n",
    "LAYER_2 = np.random.randn(ATTENTION_SIZE, 1)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 23,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "(5, 10)"
      ]
     },
     "execution_count": 23,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "ENCODER_STATES.shape"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 24,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "(1, 10)"
      ]
     },
     "execution_count": 24,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "DECODER_STATES.shape"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Methods"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 19,
   "metadata": {},
   "outputs": [],
   "source": [
    "def sigmoid(x, axis=0):\n",
    "    \"\"\"_summary_\n",
    "        axis=0 calculates softmax across rows which means each column sums to 1 \n",
    "        axis=1 calculates softmax across columns which means each row sums to 1\n",
    "\n",
    "    Args:\n",
    "        x (_type_): _description_\n",
    "        axis (int, optional): _description_. Defaults to 0.\n",
    "    \"\"\"\n",
    "    return np.exp(x) / np.expand_dims(np.sum(np.exp(x), axis=axis), axis=axis)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 1: Calculating alignment scores\n",
    "\n",
    "The first step is to calculate the alignment scores. This is a measure of similarity between the decoder hidden state and each encoder hidden state. From the paper, this operation looks like\n",
    "\n",
    "$$\n",
    "\\large e_{ij} = v_a^\\top \\tanh{\\left(W_a s_{i-1} + U_a h_j\\right)}\n",
    "$$\n",
    "\n",
    "where $W_a \\in \\mathbb{R}^{n\\times m}$, $U_a \\in \\mathbb{R}^{n \\times m}$, and $v_a \\in \\mathbb{R}^m$\n",
    "are the weight matrices and $n$ is the hidden state size. In practice, this is implemented as a feedforward neural network with two layers, where $m$ is the size of the layers in the alignment network. It looks something like:\n",
    "\n",
    "![alignment model](./images/alignment_model_3.jpg)\n",
    "\n",
    "Here $h_j$ are the encoder hidden states for each input step $j$ and $s_{i - 1}$ is the decoder hidden state of the previous step. The first layer corresponds to $W_a$ and $U_a$, while the second layer corresponds to $v_a$.\n",
    "\n",
    "To implement this, first concatenate the encoder and decoder hidden states to produce an array with size $K \\times 2n$ where $K$ is the number of encoder states/steps. For this, use `np.concatenate` ([docs](https://numpy.org/doc/stable/reference/generated/numpy.concatenate.html)). Note that there is only one decoder state so you'll need to reshape it to successfully concatenate the arrays. The easiest way is to use `decoder_state.repeat` ([docs](https://numpy.org/doc/stable/reference/generated/numpy.repeat.html#numpy.repeat)) to match the hidden state array size.\n",
    "\n",
    "Then, apply the first layer as a matrix multiplication between the weights and the concatenated input. Use the tanh function to get the activations. Finally, compute the matrix multiplication of the second layer weights and the activations. This returns the alignment scores."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def alignment(encoder_states: np.array,\n",
    "              decoder_states: np.array,\n",
    "              input_length: int) -> np.array:\n",
    "    \"\"\"_summary_\n",
    "\n",
    "    Args:\n",
    "        encoder_states (np.array): _description_\n",
    "        decoder_states (np.array): _description_\n",
    "        input_length (int): _description_\n",
    "\n",
    "    Returns:\n",
    "        np.array: _description_\n",
    "    \"\"\"\n",
    "    inputs = np.concatenate([encoder_states, decoder_states.repeat(input_length, axis=0)], axis=1)\n",
    "    assert inputs.shape == ()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If you implemented the function correctly, you should get these scores:\n",
    "\n",
    "```python\n",
    "[[4.35790943]\n",
    " [5.92373433]\n",
    " [4.18673175]\n",
    " [2.11437202]\n",
    " [0.95767155]]\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 2: Turning alignment into weights\n",
    "\n",
    "The next step is to calculate the weights from the alignment scores. These weights determine the encoder outputs that are the most important for the decoder output. These weights should be between 0 and 1. You can use the softmax function (which is already implemented above) to get these weights from the attention scores. Pass the attention scores vector to the softmax function to get the weights. Mathematically,\n",
    "\n",
    "$$\n",
    "\\large \\alpha_{ij} = \\frac{\\exp{\\left(e_{ij}\\right)}}{\\sum_{k=1}^K \\exp{\\left(e_{ik}\\right)}}\n",
    "$$\n",
    "\n",
    "\n",
    "\n",
    "## 3: Weight the encoder output vectors and sum\n",
    "\n",
    "The weights tell you the importance of each input word with respect to the decoder state. In this step, you use the weights to modulate the magnitude of the encoder vectors. Words with little importance will be scaled down relative to important words. Multiply each encoder vector by its respective weight to get the alignment vectors, then sum up the weighted alignment vectors to get the context vector. Mathematically,\n",
    "\n",
    "$$\n",
    "\\large c_i = \\sum_{j=1}^K\\alpha_{ij} h_{j}\n",
    "$$\n",
    "\n",
    "Implement these steps in the `attention` function below."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If you implemented the `attention` function correctly, the context vector should be\n",
    "\n",
    "```python\n",
    "[-0.63514569  0.04917298 -0.43930867 -0.9268003   1.01903919 -0.43181409\n",
    "  0.13365099 -0.84746874 -0.37572203  0.18279832 -0.90452701  0.17872958\n",
    " -0.58015282 -0.58294027 -0.75457577  1.32985756]\n",
    "```\n",
    "\n"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "venv_language_translation",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.10"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
